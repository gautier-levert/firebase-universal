import * as express from 'express';
import * as functions from 'firebase-functions';
import {join} from 'path';
import 'zone.js/dist/zone-node';

function isBot(userAgent: string): boolean {
  // Lists of bots to target
  const bots = [
    // crawler bots
    'Googlebot', // Google
    'AdsBot', // Google
    'bingbot', // Bing
    'BingPreview', // Bing
    'YandexBot', // Yandex
    'DuckDuckBot',
    'Slurp', // Yahoo!
    'Qwantify',
    'Sogou',
    'proximic',
    'Baiduspider',
    'Teoma',
    'AhrefsBot',
    // link bots
    'twitterbot',
    'facebookexternalhit',
    'linkedinbot',
    'embedly',
    'baiduspider',
    'pinterest',
    'slackbot',
    'vkShare',
    'facebot',
    'outbrain',
    'W3C_Validator'
  ];

  const agent = userAgent ? userAgent.toLowerCase() : '';

  if (bots.some(bot => {
    if (agent.indexOf(bot.toLowerCase()) > -1) {
      console.log(`bot detected: ${bot} / ${userAgent}`);
      return true;
    }
    return false;
  })) {
    return true;
  }

  console.log(`no bot found: ${userAgent}`);
  return false;
}

// Express server
const app = express();

const DIST_FOLDER = join(process.cwd(), 'lib/dist/browser');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP, ngExpressEngine, provideModuleMap} = require('./dist/server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', DIST_FOLDER);

// Serve static files from /browser
app.get('*.*', express.static(DIST_FOLDER, {
  maxAge: '1y'
}));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
  if (isBot(req.headers['user-agent'] as string)) {
    res.render('index', {req});
  } else {
    res.sendFile(join(DIST_FOLDER, 'index.html'));
  }
});

exports.ssr = functions.https.onRequest(app);
